const express = require('express');
const db = require('../db');
const utils = require('../utils');
const router = express.Router();

router.get('/', (req,resp) => {
    resp.send('Welcome to Book Application');
})

router.get('/findByName',(req,resp) => {
    const { author_name } = req.body;
    const statement = `SELECT * FROM Book WHERE author_name = '${author_name}'`

    const connection = db.openDatabasesConnection();
    connection.query(statement,(error,result) => {
        connection.end();
        resp.send(utils.createResult(error,result));
    })
})

router.post('/add',(req,resp) => {
    const { book_title, publisher_name, author_name} = req.body;

    
    const statement = `INSERT INTO Book ( book_title, publisher_name, author_name ) VALUES ('${book_title}','${publisher_name}','${author_name}')`;
    
    const connection = db.openDatabasesConnection();
    connection.query(statement,(error,result) => {
        connection.end();
        resp.send(utils.createResult(error,result));
    });
                                                   
});

router.delete('/delete/:id',(req,resp) => {
    const { id } = req.params
    const statement = `DELETE FROM Book WHERE book_id = ${ id }`

    const connection = db.openDatabasesConnection();
    connection.query(statement,(error,result) => {
        connection.end();
        resp.send(utils.createResult(error,result));
    });
})

router.put('/update/:id',(req,resp) => {
    const { id } = req.params
    const { publisher_name, author_name} = req.body;

    const statement = `UPDATE Book 
                        SET 
                            publisher_name = '${publisher_name}', author_name = '${author_name}'
                            WHERE book_id = ${id}`;
                            
    const connection = db.openDatabasesConnection();
    connection.query(statement,(error,result) => {
    connection.end();
    resp.send(utils.createResult(error,result));
    });

})
    


module.exports = router;