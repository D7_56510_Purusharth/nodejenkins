const express = require('express');
const cors = require('cors')
const bookRouter = require('./routes/book');

const app = express();
app.use(express.json());
app.use(cors('*'));

app.use('/book',bookRouter);

app.listen(4000, "0.0.0.0", ()=>{
    console.log('server startes at 4000');
})
